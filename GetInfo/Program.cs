﻿using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using Microsoft.Win32;

namespace InformationSecurity2
{
    class Program
    {
        static void Main(string[] args)
        {
            string computerInfo = "";
            
            //інформація про комп'ютер
            SelectQuery SqComp = new SelectQuery("Win32_OperatingSystem");
            ManagementObjectSearcher compOSDetails = new ManagementObjectSearcher(SqComp);
            ManagementObjectCollection osDetailsCollectionComp = compOSDetails.Get();
            
            computerInfo += Environment.UserName;
            computerInfo += Environment.MachineName;
            Console.WriteLine("User name: {0}", Environment.UserName);
            Console.WriteLine("Computer name: {0}", Environment.MachineName);
            

            foreach (ManagementObject comp in osDetailsCollectionComp)
            {
                computerInfo += comp["WindowsDirectory"].ToString();
                computerInfo += comp["SystemDirectory"].ToString();
                
                Console.WriteLine("Path to Windows OS: {0}", comp["WindowsDirectory"].ToString());
                Console.WriteLine("Path to Windows system files: {0}", comp["SystemDirectory"].ToString());
            }
            

            //інформація про тип і підтип клавіатури
            ManagementObjectSearcher searchKeyboardType = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_Keyboard");
            Console.WriteLine("\nInformation about keyboard : ");
            foreach (ManagementObject comp in searchKeyboardType.Get())
            {
                computerInfo += comp["Name"].ToString();
                computerInfo += comp["Description"].ToString();

                Console.WriteLine("Keyboard type: {0}", comp["Name"]);
                Console.WriteLine("Keyboard subtype: {0}", comp["Description"]);
            }

            // інформація про висоту та ширину екрану
            ManagementObjectSearcher searchScreenInfo = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_VideoController");

            System.Collections.Generic.List<Scr> scr = new System.Collections.Generic.List<Scr>();
            Console.WriteLine("\nInformation about screen");
            foreach (ManagementObject comp in searchScreenInfo.Get())
            {
                if (comp["CurrentHorizontalResolution"] != null)
                {
                    Scr screen = new Scr();
                    computerInfo += comp["CurrentVerticalResolution"].ToString();

                    screen.Width = comp["CurrentHorizontalResolution"].ToString();
                    screen.Height = comp["CurrentVerticalResolution"].ToString();
                    scr.Add(screen);
                }
            }
            foreach (var item in scr)
            {
                Console.WriteLine($"monitor height: {item.Height}\nmonitor width: {item.Width} ");
            }

            // інформація про набір дискових пристроїв
            Console.WriteLine("\nInformation about disk drives");
            foreach (var drive in DriveInfo.GetDrives())
            {
                computerInfo += drive.Name;
                Console.WriteLine("drive name: " + drive.Name);
            }

            // інформація про серійний номер
            Console.WriteLine("\nInformation about disk");
            ManagementObjectSearcher moSearcher = new ManagementObjectSearcher("SELECT * FROM Win32_DiskDrive");
            foreach (ManagementObject wmi_HD in moSearcher.Get())
            {
                computerInfo += wmi_HD["SerialNumber"].ToString();
                Console.WriteLine("Serial number:" + wmi_HD["SerialNumber"].ToString());
            }

            Console.WriteLine("\nAll computer info : {0}", computerInfo);


            var key = "983BB91D16C645BE2BD9574402AC9F8F5BC42A34";
            var encryptedComputerInfo = Encryption.EncryptHash(computerInfo);
            Console.WriteLine("Encrypted info : {0}", encryptedComputerInfo);
            SaveEncryptedInfo(encryptedComputerInfo);
            ReadEncryptedInfo(key, encryptedComputerInfo);

            Console.ReadKey();
        }

        public static void SaveEncryptedInfo(string encryptedInfo)
        {
            RegistryKey userKey = Registry.CurrentUser;
            RegistryKey personalKey = userKey.CreateSubKey("Shelyakhina_");
            personalKey.SetValue("Signature", encryptedInfo);
            personalKey.Close();
            Console.WriteLine("\nData is written successfuly\n");
        }

        public static void ReadEncryptedInfo(string key, string encryptedKey)
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey personalKey = currentUserKey.OpenSubKey("Shelyakhina_", true);

            if (encryptedKey == key)
            {
                string signedData = personalKey.GetValue("Signature").ToString();
                Console.WriteLine("Current key is appropriate.");
            }
            else
            {
                Console.WriteLine("Current key is not appropriate. Access is denied.");
            }

            personalKey.Close();
        }

        public static void DeleteKey()
        {
            RegistryKey currentUserKey = Registry.CurrentUser;
            RegistryKey delKey = currentUserKey.OpenSubKey("Shelyakhina_", true);
            delKey.DeleteValue("Signature");
            currentUserKey.DeleteSubKey("Shelyakhina_");
        }
    }


    class Scr
    {
        public string Height { get; set; }
        public string Width { get; set; }
    }
}
