﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PasGen
{
    public class PasBreak
    {
        public static void Break(bool includeLowercase, bool includeUppercase, bool includeNumeric, bool includeSpecial, bool includeSpaces, int lengthOfPassword)
        {
            bool flag = false;
            string randomPassword = string.Empty;
            string currentPassword = FileManager.ReadPass();

            HashSet<string> passwords = new HashSet<string>();
            int addedPasswordCount = 0;
            while (addedPasswordCount < 100000)
            {
                randomPassword = Generator.GenerateValidPassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);

                Console.WriteLine(randomPassword);

                if (passwords.Contains(randomPassword) == false)
                {
                    passwords.Add(randomPassword);
                    addedPasswordCount++;

                    if (randomPassword == currentPassword)
                    {
                        Console.WriteLine("SUCCESS: the password was cracked.");
                        flag = true;
                        break;
                    }
                }
            }

            if (flag == false)
            {
                Console.WriteLine("FAIL: no matches found.");
            }
        }
    }
}
