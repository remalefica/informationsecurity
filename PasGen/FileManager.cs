﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace PasGen
{
    public class FileManager
    {
        public static string ReadPass()
        {
            string text = File.ReadAllText($"{Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName}" +
                                                 $"\\loginpass.txt");
            return text;
        }

        public static void WritePass(string value)
        {
            File.WriteAllText($"{Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName}" +
                                                 $"\\loginpass.txt", value);
        }
    }
}
