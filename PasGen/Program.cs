﻿using System;
using System.Collections.Generic;
using System.IO;

namespace PasGen
{
    class Program
    {
        public static void Main(string[] args)
        {
            bool includeLowercase = true;
            bool includeUppercase = true;
            bool includeNumeric = true;
            bool includeSpecial = true;
            bool includeSpaces = false;
            int lengthOfPassword = 8;

            string password = Generator.GenerateValidPassword(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);

            FileManager.WritePass(password);

            Console.WriteLine("Generated password: " + password);

            Console.WriteLine("\n\nDo you want to break this password? Press y if yes.");
            char ans = Console.ReadKey().KeyChar;
            if (ans=='y')
            {
                Console.WriteLine("\n\n");
                PasBreak.Break(includeLowercase, includeUppercase, includeNumeric, includeSpecial, includeSpaces, lengthOfPassword);
            }

            Console.ReadKey();
        }

    }
}
