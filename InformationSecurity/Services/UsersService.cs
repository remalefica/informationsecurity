﻿using InformationSecurity.Helpers;
using InformationSecurity.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Windows;

namespace InformationSecurity.Services
{
    public class UsersService : IUsersService
    {
        private static UsersService _instance;

        private readonly string _encryptedUsersJsonPath = $"{Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName}" +
                                                 $"\\Data\\encrypted.txt";

        private readonly string _decryptedUsersJsonPath = $"{Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName}" +
                                                 $"\\Data\\decrypted.json";

        private readonly string Key = "8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO";
        private readonly List<User> _users;
        private int _loginAttempts = 0;

        private UsersService()
        {
            _users = LoadUsersFromJson();
        }

        public static IUsersService GetService()
        {
            if (_instance == null)
            {
                _instance = new UsersService();
            }

            return _instance;
        }

        public List<User> GetUsers()
        {
            return _users;
        }

        public User SignIn(string login, string password)
        {
            var user = _users.FirstOrDefault(x => x.Login == login);

            if (user == null)
            {
                throw new Exception("User with this Login does not exist");
            }

            // Deciding on the correctness of the entered password
            if (user.Password != password)
            {
                _loginAttempts++;

                // Exceeding the set start limit
                if (_loginAttempts == 3)
                {
                    ExitApplication();
                }

                throw new Exception("Password is incorrect");
            }

            if (user.IsBlocked)
            {
                throw new Exception("User is blocked");
            }
            
            // Admin can see decrypted file
            if (user.IsAdmin)
            {
                DecryptFile();
            }

            return user;
        }

        public void AddUser(User user)
        {
            if (_users.Any(x => x.Login == user.Login))
            {
                throw new Exception("User with this Login already exists");
            }

            _users.Add(user);

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        public void BlockUser(string userID)
        {
            _users.FirstOrDefault(x => x.ID == userID).IsBlocked = true;

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        public void UnblockUser(string userID)
        {
            _users.FirstOrDefault(x => x.ID == userID).IsBlocked = false;

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        public void SetPasswordPattern(string userID)
        {
            var user = _users.FirstOrDefault(x => x.ID == userID);

            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            user.IsPasswordLimitationSet = true;

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        public void UnsetPasswordPattern(string userID)
        {
            var user = _users.FirstOrDefault(x => x.ID == userID);

            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            user.IsPasswordLimitationSet = false;

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        public void ChangePassword(string userID, string newPassword, string oldPassword)
        {
            var user = _users.FirstOrDefault(x => x.ID == userID);

            if (user == null)
            {
                throw new Exception("User does not exist");
            }

            if (user.Password != oldPassword)
            {
                throw new Exception("Old password does not match");
            }

            if (!user.IsAdmin && user.IsPasswordLimitationSet && !PasswordPatternHolder.CheckPasswordPattern(newPassword))
            {
                throw new Exception("Password is in incorrect format");
            }
            else
            {
                user.Password = newPassword;
            }

            WriteToFile(_decryptedUsersJsonPath, Converter.ToJson(_users));
        }

        // Clear temp DB
        public void ExitApplication()
        {
            if (File.Exists(_decryptedUsersJsonPath)) {
                var list = ReadFromDecrypted<List<User>>();
                if (list != null)
                {
                    string json = Converter.ToJson(list);

                    string encrList = CryptService.Encrypt(json, Key);

                    WriteToFile(_encryptedUsersJsonPath, encrList);

                    ClearTemporaryDB();
                }
            }

            Application.Current.Shutdown();
        }

        private List<User> LoadUsersFromJson()
        {
            var list = new List<User>();

            if (!new FileInfo(_encryptedUsersJsonPath).Exists)
            {
                File.Create(_encryptedUsersJsonPath).Close();

                list.Add(new User
                {
                    ID = IdCreator.NewUserID(),
                    Login = "ADMIN",
                    Password = string.Empty,
                    IsAdmin = true,
                    IsBlocked = false,
                    IsPasswordLimitationSet = false
                });

                string json = Converter.ToJson(list);

                // Encrypt new file
                string encrList = CryptService.Encrypt(json, Key);

                WriteToFile(_encryptedUsersJsonPath, encrList);
            }
            else
            {
                DecryptFile();
                list = ReadFromDecrypted<List<User>>();
                ClearTemporaryDB();
            }

            return list;
        }

        private void DecryptFile()
        {
            // get hash from encrypted.json
            string hash = ReadFromEncrypted();

            // Decrypt list string
            string decrypted_str = CryptService.Decrypt(hash, Key);

            WriteToFile(_decryptedUsersJsonPath, decrypted_str);
        }

        private void WriteToFile(string file, string value)
        {
            File.WriteAllText(file, value);
        }

        private T ReadFromDecrypted<T>()
        {
            using var r = new StreamReader(_decryptedUsersJsonPath);
            string json = r.ReadToEnd();
            return Converter.FromJson<T>(json);
        }

        private string ReadFromEncrypted()
        {
            // Read the file as one string.
            string text = File.ReadAllText(_encryptedUsersJsonPath);
            return text;
        }


        // clear tmp db
        public void ClearTemporaryDB()
        {
            if (File.Exists(_decryptedUsersJsonPath))
            {
                File.WriteAllText(_decryptedUsersJsonPath, string.Empty);
            }
        }
    }
}
