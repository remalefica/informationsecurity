﻿using InformationSecurity.Models;
using System.Collections.Generic;

namespace InformationSecurity.Services
{
    public interface IUsersService
    {
        List<User> GetUsers();
        void AddUser(User user);
        void BlockUser(string userID);
        void ChangePassword(string userID, string newPassword, string oldPassword);
        void ExitApplication();
        void SetPasswordPattern(string userID);
        void UnsetPasswordPattern(string userID);
        User SignIn(string login, string password);
        void UnblockUser(string userID);
    }
}