﻿using System;

namespace InformationSecurity.Helpers
{
    public static class IdCreator
    {
        public static string NewGuid()
        {
            return Guid.NewGuid().ToString();
        }

        public static string NewUserID()
        {
            return $"user-{NewGuid()}";
        }

        public static string NewUserID(string login)
        {
            return $"user-{login}-{NewGuid()}";
        }
    }
}