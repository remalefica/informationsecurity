﻿using Newtonsoft.Json;

namespace InformationSecurity.Helpers
{
    public static class Converter
    {
        public static string ToJson<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static T FromJson<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}