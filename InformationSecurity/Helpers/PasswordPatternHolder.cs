﻿using System.Linq;

namespace InformationSecurity.Helpers
{
    public static class PasswordPatternHolder
    {
        public static string GetPattern()
        {
            return @"^[a-zA-Z0-9\-'’()/.,\s]+$";
        }

        public static bool CheckPasswordPattern(string value)
        {
            return value.Any(char.IsUpper) && value.Any(char.IsLower) && value.Any(char.IsDigit) && value.Any(x => !char.IsLetterOrDigit(x));
        }
    }
}
