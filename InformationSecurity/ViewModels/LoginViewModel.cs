﻿using InformationSecurity.Services;
using InformationSecurity.Views;
using System;
using System.Windows.Input;

namespace InformationSecurity.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private string _login;
        public string Login 
        {
            get => _login;

            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        private string _password = string.Empty;
        public string Password
        {
            get => _password;

            set
            {
                _password = value;
                OnPropertyChanged(nameof(Password));
            }
        }

        private string _errorMessage = string.Empty;
        public string ErrorMessage
        {
            get => _errorMessage;

            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        public ICommand SignInCommand { get; private set; }
        private readonly IUsersService _service;

        public event EventHandler Closing;

        public LoginViewModel()
        {
            _service = UsersService.GetService();

            SignInCommand = new RelayCommand(SignIn);
        }

        private void SignIn(object obj)
        {
            try
            {
                var user = _service.SignIn(Login, Password);

                var view = new MainView(user);
                view.Show();

                Closing?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }
    }
}
