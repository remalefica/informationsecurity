﻿using InformationSecurity.Models;
using InformationSecurity.Services;
using InformationSecurity.Views;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace InformationSecurity.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IUsersService _service;
        private readonly User _user;

        private ObservableCollection<User> _users;
        public ObservableCollection<User> Users
        {
            get => _users;
            set
            {
                _users = value;
                OnPropertyChanged(nameof(Users));
            }
        }

        private User _selectedUser;
        public User SelectedUser
        {
            get => _selectedUser;
            set
            {
                if (value != null)
                {
                    _selectedUser = value;
                    UserFormVisibility = Visibility.Visible;
                    OnPropertyChanged(nameof(SelectedUser));
                }
            }
        }

        private bool _isAdmin;
        public bool IsAdmin
        {
            get => _isAdmin;
            set
            {
                _isAdmin = value;
                OnPropertyChanged(nameof(IsAdmin));
            }
        }

        private string _greetings = string.Empty;
        public string Greetings
        {
            get => _greetings;
            set
            {
                _greetings = value;
                OnPropertyChanged(nameof(Greetings));
            }
        }

        private string _errorMessage = string.Empty;
        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        private string _newPassword = string.Empty;
        public string NewPassword
        {
            get => _newPassword;
            set
            {
                _newPassword = value;
                OnPropertyChanged(nameof(NewPassword));
            }
        }

        private string _newPasswordConfirm = string.Empty;
        public string NewPasswordConfirm
        {
            get => _newPasswordConfirm;
            set
            {
                _newPasswordConfirm = value;
                OnPropertyChanged(nameof(NewPasswordConfirm));
            }
        }

        private string _oldPassword = string.Empty;
        public string OldPassword
        {
            get => _oldPassword;
            set
            {
                _oldPassword = value;
                OnPropertyChanged(nameof(OldPassword));
            }
        }

        private Visibility _adminPageVisibility;
        public Visibility AdminPageVisibility
        {
            get => _adminPageVisibility;
            set
            {
                _adminPageVisibility = value;
                OnPropertyChanged(nameof(AdminPageVisibility));
            }
        }

        private Visibility _userPageVisibility;
        public Visibility UserPageVisibility
        {
            get => _userPageVisibility;
            set
            {
                _userPageVisibility = value;
                OnPropertyChanged(nameof(UserPageVisibility));
            }
        }

        private Visibility _userFormVisibility;
        public Visibility UserFormVisibility
        {
            get => _userFormVisibility;
            set
            {
                _userFormVisibility = value;
                OnPropertyChanged(nameof(UserFormVisibility));
            }
        }

        public ICommand ShowAboutCommand { get; private set; }
        public ICommand ChangePasswordCommand { get; private set; }
        public ICommand CreateUserCommand { get; private set; }
        public ICommand SubmitCommand { get; private set; }
        public ICommand SubmitPasswordChangeCommand { get; private set; }
        public ICommand ExitCommand { get; private set; }

        public MainViewModel(User user)
        {
            _user = user;
            _service = UsersService.GetService();

            if (_user.IsAdmin)
            {
                AdminPageVisibility = Visibility.Visible;
                UserFormVisibility = Visibility.Hidden;
                UserPageVisibility = Visibility.Hidden;

                Users = new ObservableCollection<User>(_service.GetUsers().Where(x => !x.IsAdmin));
            }
            else
            {
                AdminPageVisibility = Visibility.Hidden;
                UserFormVisibility = Visibility.Hidden;
                UserPageVisibility = Visibility.Visible;
            }

            Greetings = $"Hello, {_user.Login}";

            ShowAboutCommand = new RelayCommand(ShowAbout);
            ChangePasswordCommand = new RelayCommand(ChangePassword);
            CreateUserCommand = new RelayCommand(CreateUser);
            SubmitCommand = new RelayCommand(Submit);
            SubmitPasswordChangeCommand = new RelayCommand(SubmitPasswordChange);
            ExitCommand = new RelayCommand(Exit);
        }

        private void SubmitPasswordChange(object obj)
        {
            ErrorMessage = string.Empty;

            if (NewPassword != NewPasswordConfirm)
            {
                ErrorMessage = "Passwords don`t match";
                return;
            }

            try
            {
                _service.ChangePassword(_user.ID, NewPassword, OldPassword);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }

        private void Submit(object obj)
        {
            string userID = SelectedUser.ID;

            if (SelectedUser.IsBlocked)
            {
                _service.BlockUser(userID);
            }
            else
            {
                _service.UnblockUser(userID);
            }

            if (SelectedUser.IsPasswordLimitationSet)
            {
                _service.SetPasswordPattern(userID);
            }
            else
            {
                _service.UnsetPasswordPattern(userID);
            }
        }

        private void Exit(object obj)
        {
            _service.ExitApplication();
        }

        private void ShowAbout(object obj)
        {
            new AboutView().ShowDialog();
        }

        private void ChangePassword(object obj)
        {
            new AdminPasswordChangeView(_user).ShowDialog();
        }

        private void CreateUser(object obj)
        {
            new NewUserView(this).ShowDialog();
        }
    }
}