﻿using InformationSecurity.Helpers;
using InformationSecurity.Models;
using InformationSecurity.Services;
using System;
using System.Windows.Input;

namespace InformationSecurity.ViewModels
{
    public class NewUserViewModel : BaseViewModel
    {
        private readonly IUsersService _service;
        private readonly MainViewModel _mainViewModel;
        private string _errorMessage = string.Empty;
        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        private string _login = string.Empty;
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                OnPropertyChanged(nameof(Login));
            }
        }

        public ICommand SubmitCommand { get; private set; }
        public event EventHandler Closing;

        public NewUserViewModel(ref MainViewModel mainViewModel)
        {
            _service = UsersService.GetService();

            SubmitCommand = new RelayCommand(Submit);
            _mainViewModel = mainViewModel;
        }

        private void Submit(object obj)
        {
            var user = new User
            {
                ID = IdCreator.NewUserID(),
                Login = Login,
                Password = string.Empty,
                IsAdmin = false,
                IsBlocked = false,
                IsPasswordLimitationSet = false
            };

            try
            {
                _service.AddUser(user);
                _mainViewModel.Users.Add(user);
                Closing?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }
    }
}
