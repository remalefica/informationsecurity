﻿using InformationSecurity.Models;
using InformationSecurity.Services;
using System;
using System.Windows.Input;

namespace InformationSecurity.ViewModels
{
    public class AdminPasswordChangeViewModel : BaseViewModel
    {
        private readonly IUsersService _service;
        private readonly User _user;

        private string _errorMessage = string.Empty;
        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                _errorMessage = value;
                OnPropertyChanged(nameof(ErrorMessage));
            }
        }

        private string _newPassword = string.Empty;
        public string NewPassword
        {
            get => _newPassword;
            set
            {
                _newPassword = value;
                OnPropertyChanged(nameof(NewPassword));
            }
        }

        private string _newPasswordConfirm = string.Empty;
        public string NewPasswordConfirm
        {
            get => _newPasswordConfirm;
            set
            {
                _newPasswordConfirm = value;
                OnPropertyChanged(nameof(NewPasswordConfirm));
            }
        }

        private string _oldPassword = string.Empty;
        public string OldPassword
        {
            get => _oldPassword;
            set
            {
                _oldPassword = value;
                OnPropertyChanged(nameof(OldPassword));
            }
        }

        public ICommand SubmitCommand { get; private set; }
        public event EventHandler Closing;

        public AdminPasswordChangeViewModel(User user)
        {
            _user = user;
            _service = UsersService.GetService();

            SubmitCommand = new RelayCommand(Submit);
        }

        private void Submit(object obj)
        {
            ErrorMessage = string.Empty;

            if(NewPassword != NewPasswordConfirm)
            {
                ErrorMessage = "Passwords don`t match";
                return;
            }

            try
            {
                _service.ChangePassword(_user.ID, NewPassword, OldPassword);
                Closing?.Invoke(this, EventArgs.Empty);
            }
            catch (Exception ex)
            {
                ErrorMessage = ex.Message;
            }
        }
    }
}
