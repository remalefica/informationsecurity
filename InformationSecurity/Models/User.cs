﻿namespace InformationSecurity.Models
{
    public class User
    {
        public string ID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        //public string PasswordPattern { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsBlocked { get; set; }
        public bool IsPasswordLimitationSet { get; set; }
    }
}
