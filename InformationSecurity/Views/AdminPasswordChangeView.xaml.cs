﻿using InformationSecurity.Models;
using InformationSecurity.ViewModels;
using System.Windows;

namespace InformationSecurity.Views
{
    /// <summary>
    /// Логика взаимодействия для AdminPasswordChangeView.xaml
    /// </summary>
    public partial class AdminPasswordChangeView : Window
    {
        public AdminPasswordChangeView(User user)
        {
            InitializeComponent();

            var vm = new AdminPasswordChangeViewModel(user);
            DataContext = vm;

            vm.Closing += (s, e) => Close();
        }
    }
}
