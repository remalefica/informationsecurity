﻿using InformationSecurity.ViewModels;
using System.Windows;

namespace InformationSecurity.Views
{
    /// <summary>
    /// Логика взаимодействия для NewUserView.xaml
    /// </summary>
    public partial class NewUserView : Window
    {
        public NewUserView(MainViewModel mainViewModel)
        {
            InitializeComponent();

            var vm = new NewUserViewModel(ref mainViewModel);
            DataContext = vm;

            vm.Closing += (s, e) => Close();
        }
    }
}