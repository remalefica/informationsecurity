﻿using InformationSecurity.Models;
using InformationSecurity.ViewModels;
using System.Windows;

namespace InformationSecurity.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : Window
    {
        public MainView(User user)
        {
            InitializeComponent();

            var vm = new MainViewModel(user);
            DataContext = vm;
        }
    }
}