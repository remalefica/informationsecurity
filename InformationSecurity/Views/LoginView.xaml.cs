﻿using InformationSecurity.ViewModels;
using System.Windows;

namespace InformationSecurity.Views
{
    /// <summary>
    /// Логика взаимодействия для LoginWindow.xaml
    /// </summary>
    public partial class LoginView : Window
    {
        public LoginView()
        {
            InitializeComponent();

            var vm = new LoginViewModel();

            DataContext = vm;

            vm.Closing += (s, e) => Close();
        }
    }
}
